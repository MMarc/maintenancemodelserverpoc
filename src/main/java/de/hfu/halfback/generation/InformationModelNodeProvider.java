package de.hfu.halfback.generation;

import de.hfu.halfback.informationModel.server.*;

public class InformationModelNodeProvider {
    private final PowerComponentTypeNode engineNode;
    private final TemperatureComponentTypeNode workpieceNode;
    private final MultiComponentTypeNode cabinNode;
    private final NoiseComponentTypeNode cabinNoiseNode;
    private final NoiseComponentTypeNode cabinVibrationNode;
    private final SensorTypeNode powerSensor;
    private final SensorTypeNode temperatureSensor;
    private final SensorTypeNode noiseSensor;
    private final SensorTypeNode vibrationSensor;

    public InformationModelNodeProvider(
            PowerComponentTypeNode engineNode,
            TemperatureComponentTypeNode workpieceNode,
            MultiComponentTypeNode cabinNode,
            NoiseComponentTypeNode cabinNoiseNode,
            NoiseComponentTypeNode cabinVibrationNode,
            SensorTypeNode powerSensor,
            SensorTypeNode temperatureSensor,
            SensorTypeNode noiseSensor,
            SensorTypeNode vibrationSensor
    ){

        this.engineNode = engineNode;
        this.workpieceNode = workpieceNode;
        this.cabinNode = cabinNode;
        this.cabinNoiseNode = cabinNoiseNode;
        this.cabinVibrationNode = cabinVibrationNode;
        this.powerSensor = powerSensor;
        this.temperatureSensor = temperatureSensor;
        this.noiseSensor = noiseSensor;
        this.vibrationSensor = vibrationSensor;
    }

    public PowerComponentTypeNode getEngineNode() {
        return engineNode;
    }

    public TemperatureComponentTypeNode getWorkpieceNode() {
        return workpieceNode;
    }

    public MultiComponentTypeNode getCabinNode() {
        return cabinNode;
    }

    public NoiseComponentTypeNode getCabinNoiseNode() {
        return cabinNoiseNode;
    }

    public NoiseComponentTypeNode getCabinVibrationNode() {
        return cabinVibrationNode;
    }

    public SensorTypeNode getPowerSensor() {
        return powerSensor;
    }

    public SensorTypeNode getTemperatureSensor() {
        return temperatureSensor;
    }

    public SensorTypeNode getNoiseSensor() {
        return noiseSensor;
    }

    public SensorTypeNode getVibrationSensor() {
        return vibrationSensor;
    }
}
