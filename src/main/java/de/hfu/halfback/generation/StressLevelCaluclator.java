package de.hfu.halfback.generation;

import com.prosysopc.ua.nodes.DataChangeListener;
import com.prosysopc.ua.nodes.UaNode;
import de.hfu.halfback.informationModel.server.ComponentTypeNode;
import de.hfu.halfback.informationModel.server.SensorTypeNode;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.UnsignedShort;
import org.opcfoundation.ua.core.Range;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class StressLevelCaluclator {
    private final ComponentTypeNode componentNode;
    private final Callable<Range> variableRangeFunc;
    private final SensorTypeNode sensorNode;

    public static UnsignedShort normalStressLevel = new UnsignedShort().add(32768);
    public static UnsignedShort highStressLevel = new UnsignedShort().add(65535);
    public static UnsignedShort lowStressLevel = new UnsignedShort().add(0);

    private org.slf4j.Logger logger = LoggerFactory.getLogger(StressLevelCaluclator.class);

    private final DataChangeListener listener = new DataChangeListener() {

        @Override
        public void onDataChange(UaNode uaNode, DataValue prevValue, DataValue value) {
            Range range;
            try {
                range = variableRangeFunc.call();
            } catch (Exception e) {
                logger.error("Failed to get range of " + componentNode.getDisplayName());
                return;
            }
            double doubleValue = value.getValue().doubleValue();
            if (range.getHigh() < doubleValue){
                componentNode.setStressLevel(highStressLevel);
            }
            if (range.getHigh() > doubleValue && range.getLow() < doubleValue){
                componentNode.setStressLevel(normalStressLevel);
            }
            if(range.getLow() > doubleValue){
                componentNode.setStressLevel(lowStressLevel);
            }
        }
    };

    public StressLevelCaluclator(ComponentTypeNode componentNode, Callable<Range> valueRangeFunc, SensorTypeNode sensorNode){

        this.componentNode = componentNode;
        this.variableRangeFunc = valueRangeFunc;
        this.sensorNode = sensorNode;

        this.sensorNode.getValueNode().addDataChangeListener(listener);
    }
}
