package de.hfu.halfback.generation;

import com.prosysopc.ua.nodes.DataChangeListener;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.server.NodeManagerUaNode;
import de.hfu.halfback.informationModel.server.ComponentTypeNode;
import de.hfu.halfback.informationModel2.server.HighStressLevelChangeEventTypeNode;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.slf4j.LoggerFactory;

public class EventGenerator {
    private final ComponentTypeNode componentToObserve;
    private final NodeManagerUaNode nodeManagerUaNode;

    private org.slf4j.Logger logger = LoggerFactory.getLogger(EventGenerator.class);

    private final DataChangeListener listener = new DataChangeListener() {

        @Override
        public void onDataChange(UaNode uaNode, DataValue prevValue, DataValue value) {
            if (value.getValue().intValue() > StressLevelCaluclator.normalStressLevel.intValue()){
                try
                {
                    HighStressLevelChangeEventTypeNode event = nodeManagerUaNode.createEvent(HighStressLevelChangeEventTypeNode.class);

                    event.setSource(componentToObserve);
                    event.setMessage("High StressLevel detected on Component " + componentToObserve.getDisplayName());
                    event.setStressLevel(value.getValue().doubleValue());
                    event.triggerEvent(null);
                    logger.info("Fired High StressLevel of Component " + componentToObserve.getDisplayName());
                }
                catch (Exception ex){
                    logger.error("Failed to send high stress level event of " + componentToObserve.getDisplayName(), ex);
                }
            }
        }
    };

    public EventGenerator(ComponentTypeNode componentToObserve, NodeManagerUaNode nodeManagerUaNode){

        this.componentToObserve = componentToObserve;
        this.nodeManagerUaNode = nodeManagerUaNode;

        this.componentToObserve.getStressLevelNode().addDataChangeListener(listener);
    }
}
