package de.hfu.halfback.generation;

import com.prosysopc.ua.nodes.DataChangeListener;
import com.prosysopc.ua.nodes.UaNode;
import de.hfu.halfback.informationModel.server.ComponentTypeNode;
import de.hfu.halfback.informationModel.server.MultiComponentTypeNode;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.slf4j.LoggerFactory;

public class AggregatedStressLevelCalculator {
    private final MultiComponentTypeNode target;

    private org.slf4j.Logger logger = LoggerFactory.getLogger(AggregatedStressLevelCalculator.class);


    private final DataChangeListener listener = new DataChangeListener() {

        @Override
        public synchronized void onDataChange(UaNode uaNode, DataValue prevValue, DataValue value) {

            int currentValue = 0;
            try
            {
                currentValue = target.getStressLevel().intValue();
            }catch(Exception ex){
                logger.error("Failed to calculate stress level for " + target.getDisplayName(), ex);
                return;
            }

            currentValue -= prevValue.getValue().intValue();
            currentValue += value.getValue().intValue();

            if (currentValue < 0){
                target.setStressLevel(StressLevelCaluclator.lowStressLevel);
            }
            if (currentValue > 0 && currentValue < 32768){
                target.setStressLevel(StressLevelCaluclator.normalStressLevel);
            }
            if (currentValue > 32768){
                target.setStressLevel(StressLevelCaluclator.highStressLevel);
            }
        }
    };

    public AggregatedStressLevelCalculator(MultiComponentTypeNode target){

        this.target = target;

        for (UaNode subComponent :
                this.target.getComponents()) {
            if (subComponent instanceof ComponentTypeNode){
                ((ComponentTypeNode)subComponent).getStressLevelNode().addDataChangeListener(listener);
            }
        }
    }
}
