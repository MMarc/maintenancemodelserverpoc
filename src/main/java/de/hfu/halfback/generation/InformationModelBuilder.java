package de.hfu.halfback.generation;

import com.prosysopc.ua.CodegenModel;
import com.prosysopc.ua.nodes.UaInstance;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaType;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.types.opcua.FolderType;
import de.hfu.halfback.helper.EnableAllIoManagerListener;
import de.hfu.halfback.MaintenanceModelNodeIds;
import de.hfu.halfback.exceptions.ModelInitializationFailed;
import de.hfu.halfback.informationModel.server.*;
import de.hfu.halfback.informationModel2.server.MaintenanceInterfaceTypeNode;
import de.hfu.halfback.server.OPCUAServer;
import de.hfu.halfback.server.ServerConfiguration;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedShort;
import org.opcfoundation.ua.common.NamespaceTable;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.Range;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

public class InformationModelBuilder {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(InformationModelBuilder.class);
    private OPCUAServer server;
    private int maintenanceNamespaceModelIndex;
    private NodeManagerUaNode nodeManagerUaNode;
    private UaType baseObjectType;
    private FolderType objectsFolder;
    private MaintenanceInterfaceTypeNode maintenanceInterfaceTypeNode;
    private ComponentsListTypeNode componentListTypeNode;
    private SensorListTypeNode sensorListTypeNode;
    private PowerComponentTypeNode engineComponentTypeNode;
    private TemperatureComponentTypeNode workpieceComponentTypeNode;
    private MultiComponentTypeNode cabineComponentTypeNode;
    private NoiseComponentTypeNode noiseComponentTypeNode;
    private NoiseComponentTypeNode vibrationComponentTypeNode;

    private SensorTypeNode powerSensorTypeNode;
    private SensorTypeNode temperatureSensorTypeNode;
    private SensorTypeNode noiseSensorTypeNode;
    private SensorTypeNode vibrationSensorTypeNode;

    public void build(OPCUAServer server) throws ModelInitializationFailed {
        this.server = server;
        initializeRequiredBaseNodes();
        importInformationModelFromFilePart("maintenancemodel.xml", de.hfu.halfback.informationModel.server.InformationModel.MODEL);
        importInformationModelFromFilePart("maintenancemodel2.xml", de.hfu.halfback.informationModel2.server.InformationModel.MODEL);
        createModelInAddressSpace();
        nodeManagerUaNode.getIoManager().setListeners(new EnableAllIoManagerListener());
    }

    private void initializeRequiredBaseNodes() throws ModelInitializationFailed {
        nodeManagerUaNode = new NodeManagerUaNode(server.getUaServer(),
                ServerConfiguration.ServerNodeNamespace);
        try
        {
            baseObjectType = server.getUaServer().getNodeManagerRoot().getType(
                    Identifiers.BaseObjectType);
            objectsFolder = server.getUaServer().getNodeManagerRoot().getObjectsFolder();
        }
        catch (Exception ex){
            logger.error("Failed to create object type node", ex);
            throw new ModelInitializationFailed();
        }
    }

    private void importInformationModelFromFilePart(String fileName, CodegenModel model) throws ModelInitializationFailed {
        server.getUaServer().registerModel(model);
        try {
            URL resource = getClass().getResource("/" + fileName);
            server.getUaServer().getAddressSpace().loadModel(resource.toURI());
            NamespaceTable namespaceTable =
                    server.getUaServer().getAddressSpace().getNamespaceTable();

            maintenanceNamespaceModelIndex = namespaceTable.getIndex(ServerConfiguration.ServerNodeNamespace);

            logger.info("Imported " + fileName + " into namespace nr " + maintenanceNamespaceModelIndex);
        } catch (Exception e) {
            logger.error("Failed to load model from file " + fileName, e);
            throw new ModelInitializationFailed();
        }
    }

    private void createModelInAddressSpace() throws ModelInitializationFailed {
        createMaintenanceInterfaceInstance();
        createComponentListInstance();
        createSensorListInstance();
        createComponents();
        createSensors();
        createHasMeteringReferences();
        createCustomReferences();
        createNotificationReferences();
    }

    private void createMaintenanceInterfaceInstance() throws ModelInitializationFailed {
        maintenanceInterfaceTypeNode = createInstance(
                MaintenanceInterfaceTypeNode.class,
                MaintenanceModelNodeIds.MaintenanceInterface,
                objectsFolder,
                Identifiers.HasComponent);
    }

    private void createComponentListInstance() throws ModelInitializationFailed {
        componentListTypeNode = createInstance(
                ComponentsListTypeNode.class,
                MaintenanceModelNodeIds.ComponentsList,
                maintenanceInterfaceTypeNode,
                Identifiers.HasComponent);
    }

    private void createSensorListInstance() throws ModelInitializationFailed {
        sensorListTypeNode = createInstance(
                SensorListTypeNode.class,
                MaintenanceModelNodeIds.SensorList,
                maintenanceInterfaceTypeNode,
                Identifiers.HasComponent);
    }

    private void createComponents() throws ModelInitializationFailed {
        createEngineComponent();
        createWorkpieceComponent();
        createCabinComponent();
    }

    private void createEngineComponent() throws ModelInitializationFailed {
        engineComponentTypeNode = createInstance(
                PowerComponentTypeNode.class,
                MaintenanceModelNodeIds.EngineComponent,
                componentListTypeNode,
                Identifiers.HasComponent);
        engineComponentTypeNode.setAmpereRange(new Range(0.0,5.0));
        engineComponentTypeNode.setVoltRange(new Range(0.0,230.0));
        engineComponentTypeNode.setPowerRange(new Range(0.0,100.0));
        initializeStressLevel(engineComponentTypeNode);
    }

    private void createWorkpieceComponent() throws ModelInitializationFailed {
        workpieceComponentTypeNode = createInstance(
                TemperatureComponentTypeNode.class,
                MaintenanceModelNodeIds.WorkpieceComponent,
                componentListTypeNode,
                Identifiers.HasComponent);
        workpieceComponentTypeNode.setTemperatureRange(new Range(0.0,100.0));
        initializeStressLevel(workpieceComponentTypeNode);
    }

    private void createCabinComponent() throws ModelInitializationFailed {
        cabineComponentTypeNode = createInstance(
                MultiComponentTypeNode.class,
                MaintenanceModelNodeIds.CabinComponent,
                componentListTypeNode,
                Identifiers.HasComponent);
        createNoiseComponent();
        createVibrationComponent();
        initializeStressLevel(cabineComponentTypeNode);
    }

    private void createNoiseComponent() throws ModelInitializationFailed {
        noiseComponentTypeNode = createInstance(
                NoiseComponentTypeNode.class,
                MaintenanceModelNodeIds.NoiseCabineSubcomponent,
                cabineComponentTypeNode,
                Identifiers.HasComponent);
        noiseComponentTypeNode.setNoiseRange(new Range(0.0,100.0));
        initializeStressLevel(noiseComponentTypeNode);
    }

    private void createVibrationComponent() throws ModelInitializationFailed {
        vibrationComponentTypeNode = createInstance(
                NoiseComponentTypeNode.class,
                MaintenanceModelNodeIds.VibrationCabineSubcomponent,
                cabineComponentTypeNode,
                Identifiers.HasComponent);
        vibrationComponentTypeNode.setNoiseRange(new Range(0.0,100.0));
        initializeStressLevel(vibrationComponentTypeNode);
    }

    private void createSensors() throws ModelInitializationFailed {
        createPowerSensor();
        createTemperatureSensor();
        createNoiseSensor();
        createVibrationSensor();
    }

    private void createPowerSensor() throws ModelInitializationFailed {
        powerSensorTypeNode = createInstance(
                SensorTypeNode.class,
                MaintenanceModelNodeIds.PowerSensor,
                sensorListTypeNode,
                Identifiers.HasComponent);
        powerSensorTypeNode.setValue(0.0);
    }

    private void createTemperatureSensor() throws ModelInitializationFailed {
        temperatureSensorTypeNode = createInstance(
                SensorTypeNode.class,
                MaintenanceModelNodeIds.TemperatureSensor,
                sensorListTypeNode,
                Identifiers.HasComponent);
        temperatureSensorTypeNode.setValue(0.0);
    }

    private void createNoiseSensor() throws ModelInitializationFailed {
        noiseSensorTypeNode = createInstance(
                SensorTypeNode.class,
                MaintenanceModelNodeIds.NoiseSensor,
                sensorListTypeNode,
                Identifiers.HasComponent);
        noiseSensorTypeNode.setValue(0.0);
    }

    private void createVibrationSensor() throws ModelInitializationFailed {
        vibrationSensorTypeNode = createInstance(
                SensorTypeNode.class,
                MaintenanceModelNodeIds.VibrationSensor,
                sensorListTypeNode,
                Identifiers.HasComponent);
        vibrationSensorTypeNode.setValue(0.0);
    }

    private <T extends UaInstance> T createInstance(Class<T> classToInstantiate, String nodeIdForInstance, UaNode parentNode, NodeId referenceNodeId) throws ModelInitializationFailed {
        try
        {
            T instance = nodeManagerUaNode.createInstance(classToInstantiate, nodeIdForInstance);
            nodeManagerUaNode.addNodeAndReference(parentNode, instance, referenceNodeId);
            logger.info("Successfully created " + classToInstantiate.toString() +" instance");
            return instance;
        }
        catch (Exception ex){
            logger.error("Failed to create " + classToInstantiate.toString() +"  instance", ex);
            throw new ModelInitializationFailed();
        }
    }

    private void createHasMeteringReferences() {
        engineComponentTypeNode.addReference(
                powerSensorTypeNode,
                MaintenanceModelNodeIds.HasMetering(maintenanceNamespaceModelIndex),
                false);
        workpieceComponentTypeNode.addReference(
                temperatureSensorTypeNode,
                MaintenanceModelNodeIds.HasMetering(maintenanceNamespaceModelIndex),
                false);

        noiseComponentTypeNode.addReference(
                noiseSensorTypeNode,
                MaintenanceModelNodeIds.HasMetering(maintenanceNamespaceModelIndex),
                false);

        vibrationComponentTypeNode.addReference(
                vibrationSensorTypeNode,
                MaintenanceModelNodeIds.HasMetering(maintenanceNamespaceModelIndex),
                false);
    }

    private void createCustomReferences() {
        engineComponentTypeNode.addReference(
                workpieceComponentTypeNode,
                MaintenanceModelNodeIds.HasImpactOn(maintenanceNamespaceModelIndex),
                false);

        workpieceComponentTypeNode.addReference(
                cabineComponentTypeNode,
                MaintenanceModelNodeIds.HasQualityIndicator(maintenanceNamespaceModelIndex),
                false);
    }

    private void createNotificationReferences() {
        addHasNotifierReference(maintenanceInterfaceTypeNode, componentListTypeNode);
        addHasNotifierReference(componentListTypeNode, engineComponentTypeNode);
        addHasNotifierReference(componentListTypeNode, workpieceComponentTypeNode);
        addHasNotifierReference(componentListTypeNode, cabineComponentTypeNode);
        addHasNotifierReference(componentListTypeNode, noiseComponentTypeNode);
        addHasNotifierReference(componentListTypeNode, vibrationComponentTypeNode);
    }


    private void initializeStressLevel(ComponentTypeNode componentTypeNode){
        componentTypeNode.setStressLevel(new UnsignedShort().add(0));
    }

    private void addHasNotifierReference(UaNode source, UaNode target){
        source.addReference(target, Identifiers.HasNotifier, false);
    }

    public InformationModelNodeProvider getNodeProvider() {
        return new InformationModelNodeProvider(
                engineComponentTypeNode,
                workpieceComponentTypeNode,
                cabineComponentTypeNode,
                noiseComponentTypeNode,
                vibrationComponentTypeNode,
                powerSensorTypeNode,
                temperatureSensorTypeNode,
                noiseSensorTypeNode,
                vibrationSensorTypeNode);
    }

    public NodeManagerUaNode getNodeManagerUaNode(){
        return nodeManagerUaNode;
    }
}
