/**
 * Prosys OPC UA Java SDK
 *
 * Copyright (c) Prosys PMS Ltd., http://www.prosysopc.com.
 * All rights reserved.
 */

/**
 * DO NOT EDIT THIS FILE DIRECTLY! It is generated and will be overwritten on regeneration.
*/

package de.hfu.halfback.informationModel2.client;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpace;
import com.prosysopc.ua.nodes.UaProperty;
import com.prosysopc.ua.nodes.UaVariable;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.QualifiedName;
import org.opcfoundation.ua.builtintypes.UnsignedShort;
import de.hfu.halfback.informationModel2.client.LocationObjectTypeImpl;
import de.hfu.halfback.informationModel2.CADLocationType;

import org.opcfoundation.ua.builtintypes.ServiceResponse;
import org.opcfoundation.ua.transport.AsyncResult;

import com.prosysopc.ua.nodes.Mandatory;
import com.prosysopc.ua.nodes.Optional;
  
public abstract class CADLocationTypeImplBase extends LocationObjectTypeImpl implements CADLocationType {
	protected CADLocationTypeImplBase(AddressSpace addressSpace, NodeId nodeId, QualifiedName browseName, LocalizedText displayName) {
		super(addressSpace, nodeId, browseName, displayName);
	}
  
	@Mandatory
	@Override
	public UaProperty getCadLocationNode() {
		QualifiedName browseName = getQualifiedName("http://hs-furtwangen.de/MaintenanceModel2/", "CADLocation");
		return getProperty(browseName);
	}

	@Mandatory
	@Override
	public UnsignedShort getCadLocation() {
		UaProperty node = getCadLocationNode();
		if (node == null)
			return null;	
		Object value = node.getValue().getValue().getValue();	
		return (UnsignedShort) value;
	}
	  
	@Mandatory
	@Override
	public void setCadLocation(UnsignedShort value) throws StatusException {
		UaVariable node = getCadLocationNode();
		if (node==null){
			throw new RuntimeException("Setting CadLocation failed, the Optional node does not exist");
		}
		node.setValue(value);
	}


	

}
