package de.hfu.halfback.helper;

import de.hfu.halfback.exceptions.OutOfRangeException;
import de.hfu.halfback.exceptions.OutOfRangeSpecifier;

public class LinearValueCalculator {

    private final double m;
    private final double b;
    private final double end;
    private final double start;

    public LinearValueCalculator(double measuredValueFrom,
                                 double measuredValueTo,
                                 double actualValueFrom,
                                 double actualValueTo){
        this.start = measuredValueFrom;
        this.end = measuredValueTo;

        m = (actualValueTo-actualValueFrom)/(measuredValueTo-measuredValueFrom);
        b = actualValueFrom - m * measuredValueFrom;
    }

    public double calculate(int value) throws OutOfRangeException {
        return calculate((double) value);
    }

    public double calculate(double value) throws OutOfRangeException {
        return (m * value) + b;
    }
}
