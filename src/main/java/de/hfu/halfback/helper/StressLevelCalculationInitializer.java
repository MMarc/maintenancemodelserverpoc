package de.hfu.halfback.helper;

import de.hfu.halfback.generation.AggregatedStressLevelCalculator;
import de.hfu.halfback.generation.InformationModelNodeProvider;
import de.hfu.halfback.generation.StressLevelCaluclator;
import de.hfu.halfback.informationModel.server.ComponentTypeNode;
import de.hfu.halfback.informationModel.server.MultiComponentTypeNode;
import de.hfu.halfback.informationModel.server.SensorTypeNode;
import org.opcfoundation.ua.core.Range;

import java.util.LinkedList;
import java.util.concurrent.Callable;

public class StressLevelCalculationInitializer {
    private final InformationModelNodeProvider informationModelNodeProvider;

    private LinkedList<StressLevelCaluclator> stressLevelCaluclators = new LinkedList<StressLevelCaluclator>();

    private LinkedList<AggregatedStressLevelCalculator> aggregatedStressLevelCalculators = new LinkedList<AggregatedStressLevelCalculator>();

    public StressLevelCalculationInitializer(InformationModelNodeProvider informationModelNodeProvider){

        this.informationModelNodeProvider = informationModelNodeProvider;
    }

    public void initialize() {
        createNewStresslevelCalulator(
                informationModelNodeProvider.getEngineNode(),
                new Callable<Range>() {
                    @Override
                    public Range call() throws Exception {
                        return informationModelNodeProvider.getEngineNode().getAmpereRange();
                    }
                },
                informationModelNodeProvider.getPowerSensor());

        createNewStresslevelCalulator(
                informationModelNodeProvider.getWorkpieceNode(),
                new Callable<Range>() {
                    @Override
                    public Range call() throws Exception {
                        return informationModelNodeProvider.getWorkpieceNode().getTemperatureRange();
                    }
                },
                informationModelNodeProvider.getTemperatureSensor());

        createNewStresslevelCalulator(
                informationModelNodeProvider.getCabinNoiseNode(),
                new Callable<Range>() {
                    @Override
                    public Range call() throws Exception {
                        return informationModelNodeProvider.getCabinNoiseNode().getNoiseRange();
                    }
                },
                informationModelNodeProvider.getNoiseSensor());

        createNewStresslevelCalulator(
                informationModelNodeProvider.getCabinVibrationNode(),
                new Callable<Range>() {
                    @Override
                    public Range call() throws Exception {
                        return informationModelNodeProvider.getCabinVibrationNode().getNoiseRange();
                    }
                },
                informationModelNodeProvider.getVibrationSensor());

        createNewAggregatedStresslevelCalcualator(
                informationModelNodeProvider.getCabinNode());
    }

    private void createNewStresslevelCalulator(ComponentTypeNode componentNode, Callable<Range> valueRangeFunc, SensorTypeNode sensorNode){
        stressLevelCaluclators.add(new StressLevelCaluclator(componentNode, valueRangeFunc, sensorNode));
    }


    private void createNewAggregatedStresslevelCalcualator(MultiComponentTypeNode targetNode) {
        aggregatedStressLevelCalculators.add(new AggregatedStressLevelCalculator(targetNode));
    }
}
