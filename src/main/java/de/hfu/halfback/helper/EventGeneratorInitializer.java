package de.hfu.halfback.helper;

import com.prosysopc.ua.server.NodeManagerUaNode;
import de.hfu.halfback.generation.EventGenerator;
import de.hfu.halfback.generation.InformationModelNodeProvider;
import de.hfu.halfback.informationModel.server.ComponentTypeNode;

import java.util.LinkedList;

public class EventGeneratorInitializer {
    private final InformationModelNodeProvider informationModelNodeProvider;
    private final NodeManagerUaNode nodeManagerUaNode;

    private LinkedList<EventGenerator> eventGenerators = new LinkedList<EventGenerator>();

    public EventGeneratorInitializer(InformationModelNodeProvider informationModelNodeProvider,
                                     NodeManagerUaNode nodeManagerUaNode){
        this.informationModelNodeProvider = informationModelNodeProvider;

        this.nodeManagerUaNode = nodeManagerUaNode;
    }

    public void initialize() {
        createEventGenerator(informationModelNodeProvider.getEngineNode());
        createEventGenerator(informationModelNodeProvider.getWorkpieceNode());
        createEventGenerator(informationModelNodeProvider.getCabinNode());
        createEventGenerator(informationModelNodeProvider.getCabinNoiseNode());
        createEventGenerator(informationModelNodeProvider.getCabinVibrationNode());
    }

    private void createEventGenerator(ComponentTypeNode componentTypeNode){
        eventGenerators.add(new EventGenerator(componentTypeNode, nodeManagerUaNode));
    }
}
