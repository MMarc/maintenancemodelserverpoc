package de.hfu.halfback.collector;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import de.hfu.halfback.exceptions.OutOfRangeException;
import de.hfu.halfback.generation.InformationModelNodeProvider;
import de.hfu.halfback.helper.LinearValueCalculator;
import de.hfu.halfback.informationModel.server.SensorTypeNode;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GPIOBusDataProvider {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(GPIOBusDataProvider.class);

    private static final byte DeviceAddr = (byte) 0x48;
    private static final byte CommandAddr = (byte) 0x45;
    private static final int Value = 128;

    private static final byte A0 = (byte) 0x41;
    private static final byte A1 = (byte) 0x42;
    private static final byte A2 = (byte) 0x43;
    private static final byte A3 = (byte) 0x44;

    private SensorTypeNode powerSensor;
    private SensorTypeNode temperatureSensor;
    private SensorTypeNode noiseSensor;
    private SensorTypeNode vibrationSensor;
    private LinearValueCalculator powerSensorValueCalculator;
    private LinearValueCalculator temperatureSensorValueCalculator;
    private LinearValueCalculator noiseSensorValueCalculator;
    private LinearValueCalculator vibrationSensorValueCalculator;

    private final InformationModelNodeProvider nodeProvider;
    private I2CBus i2c;
    private I2CDevice device;

    public GPIOBusDataProvider(InformationModelNodeProvider nodeProvider){

        this.nodeProvider = nodeProvider;
    }

    public void initialize() throws IOException, I2CFactory.UnsupportedBusNumberException {
        logger.info("GPIO initialization starting");
        powerSensor = nodeProvider.getPowerSensor();
        powerSensorValueCalculator = new LinearValueCalculator(0.0,100000.0,10.0,100.0);
        temperatureSensor = nodeProvider.getTemperatureSensor();
        temperatureSensorValueCalculator = new LinearValueCalculator(0.0,1000.0,10.0,100.0);
        noiseSensor = nodeProvider.getNoiseSensor();
        noiseSensorValueCalculator = new LinearValueCalculator(0.0,100000.0,10.0,100.0);
        vibrationSensor = nodeProvider.getVibrationSensor();
        vibrationSensorValueCalculator = new LinearValueCalculator(0.0,100000.0,10.0,100.0);

        i2c = I2CFactory.getInstance(I2CBus.BUS_1);

        device = i2c.getDevice(DeviceAddr);

        //python: bus.write_byte_data(address,cmd,value) ?

        //device.write(CommandAddr);
        device.write(Value, CommandAddr);

        logger.info("GPIO initialization done");
    }

    public void run() throws IOException, InterruptedException {
        logger.info("GPIODataProvider start");
        int readValue;
        while(true){
            logger.info("Reading data");
            readValue = device.read(A0);
            logValue("A0", readValue);
            trySetValueForPowerSensor(readValue);
            readValue = device.read(A1);
            logValue("A1", readValue);
            trySetValueForTemperatureSensor(readValue);
            readValue = device.read(A2);
            logValue("A2", readValue);
            trySetValueForNoiseSensor(readValue);
            readValue = device.read(A3);
            logValue("A3", readValue);
            trySetValueForVibrationSensor(readValue);
            Thread.sleep(1000);
        }
    }

    private void logValue(String valueAdr, int value){
        logger.info(valueAdr + " sampled value: " + value);
    }

    private void trySetValueForPowerSensor(int readValue){
        powerSensor.setValue(tryCalculateValue(
                readValue,
                powerSensorValueCalculator,
                powerSensor.getDisplayName().toString()
        ));
    }

    private void trySetValueForTemperatureSensor(int readValue) {
        temperatureSensor.setValue(tryCalculateValue(
                readValue,
                temperatureSensorValueCalculator,
                temperatureSensor.getDisplayName().toString()
        ));
    }

    private void trySetValueForNoiseSensor(int readValue) {
        noiseSensor.setValue(tryCalculateValue(
                readValue,
                noiseSensorValueCalculator,
                noiseSensor.getDisplayName().toString()
        ));
    }

    private void trySetValueForVibrationSensor(int readValue) {
        vibrationSensor.setValue(tryCalculateValue(
                readValue,
                vibrationSensorValueCalculator,
                vibrationSensor.getDisplayName().toString()
        ));
    }

    private double tryCalculateValue(int value, LinearValueCalculator calculator, String sensorName){
        try {
            return calculator.calculate(value);
        } catch (OutOfRangeException e) {
            logger.error("Failed to calculate value for " + sensorName + " error:" + e.getErrorDescription(), e);
            return 0.0;
        }
    }
}
