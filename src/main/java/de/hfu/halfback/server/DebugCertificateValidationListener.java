package de.hfu.halfback.server;

import com.prosysopc.ua.CertificateValidationListener;
import com.prosysopc.ua.PkiFileBasedCertificateValidator;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.transport.security.Cert;

import java.util.EnumSet;


/**
 *  Accepts all certificates of all clients.
 */
public class DebugCertificateValidationListener implements CertificateValidationListener {
    @Override
    public PkiFileBasedCertificateValidator.ValidationResult onValidate(Cert cert, ApplicationDescription applicationDescription, EnumSet<PkiFileBasedCertificateValidator.CertificateCheck> enumSet) {
        return PkiFileBasedCertificateValidator.ValidationResult.AcceptPermanently;
    }
}
