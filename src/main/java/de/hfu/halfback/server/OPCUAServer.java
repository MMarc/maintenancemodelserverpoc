package de.hfu.halfback.server;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.PkiFileBasedCertificateValidator;
import com.prosysopc.ua.UaApplication;
import com.prosysopc.ua.server.NodeManagerRoot;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import de.hfu.halfback.exceptions.ServerInitializationFailed;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.UserTokenPolicy;
import org.opcfoundation.ua.utils.EndpointUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class OPCUAServer {
    private UaServer uaServer;
    private static Logger logger = LoggerFactory.getLogger(OPCUAServer.class);

    public OPCUAServer() {
        this.uaServer = new UaServer();
    }

    public void initialize() throws ServerInitializationFailed {
        ApplicationDescription applicationDescription = new ApplicationDescription();
        applicationDescription.setApplicationName(
                new LocalizedText(ServerConfiguration.ApplicationName + "@" + ServerConfiguration.HostName));
        applicationDescription.setApplicationUri(ServerConfiguration.ApplicationUri);
        applicationDescription.setProductUri(ServerConfiguration.ProductUri);
        applicationDescription.setApplicationType(ApplicationType.Server);

        final PkiFileBasedCertificateValidator certificateValidator = new PkiFileBasedCertificateValidator();
        uaServer.setCertificateValidator(certificateValidator);
        certificateValidator.setValidationListener(new DebugCertificateValidationListener());

        ApplicationIdentity applicationIdentity = createApplicationIdentity(applicationDescription, certificateValidator);
        uaServer.setApplicationIdentity(applicationIdentity);

        tryInitializeServer();

        logger.info("Server initialized");
    }

    private void tryInitializeServer() throws ServerInitializationFailed {
        try {
            uaServer.setPort(UaApplication.Protocol.OpcTcp, ServerConfiguration.ServerPort);

            uaServer.setServerName(ServerConfiguration.ApplicationName);

            uaServer.setBindAddresses(EndpointUtil.getInetAddresses(uaServer.isEnableIPv6()));
            uaServer.addUserTokenPolicy(UserTokenPolicy.ANONYMOUS);

            uaServer.init();
        }
        catch(Exception ex){
            logger.error("Failed to initialize server");
            throw new ServerInitializationFailed();
        }

    }

    private ApplicationIdentity createApplicationIdentity(ApplicationDescription applicationDescription, PkiFileBasedCertificateValidator certificateValidator) throws ServerInitializationFailed {
        try {
            ApplicationIdentity applicationIdentity = ApplicationIdentity.loadOrCreateCertificate(
                    applicationDescription,
                    ServerConfiguration.Organization,
                    ServerConfiguration.ServerCertSecret,
                    new File(certificateValidator.getBaseDir(), "private"),
                    true);
            logger.info("Created application certificate");
            return applicationIdentity;

        } catch (Exception ex) {
            logger.error("Certificate creation failed", ex);
            throw new ServerInitializationFailed();
        }
    }

    public void start() {
        try {
            uaServer.start();
        } catch (UaServerException ex) {
            logger.error("Failed to start server", ex);
        }
    }

    public void stop() {
        try {
            uaServer.shutdown(0, "");
        } catch (Exception ex) {
            logger.error("Failed to shutdown server", ex);
        }
    }

    public void restart() {
        stop();
        start();
    }

    public NodeManagerRoot getNodeManagerRoot() {
        return uaServer.getNodeManagerRoot();
    }

    public UaServer getUaServer() {
        return uaServer;
    }
}
