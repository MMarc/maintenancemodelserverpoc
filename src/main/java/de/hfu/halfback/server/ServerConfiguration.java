package de.hfu.halfback.server;

public class ServerConfiguration {
    public static final String ApplicationName  = "MaintenanceModelServer";
    public static final String HostName = "localhost";
    public static final String ApplicationUri = "urn:" + HostName + ":OPCUA:" + ApplicationName;
    public static final String Organization = "hs-furtwangen.de";
    public static final String ProductUri = "urn:" + Organization + ":OPCUA:" + ApplicationName;
    public static final String ServerCertSecret = "Bz5pyeobIDOsXR8VDNhN";
    public static final int ServerPort = 50000;
    public static final String ServerNodeNamespace = "http://" + Organization + "/MaintenanceModel/";
}
