package de.hfu.halfback.exceptions;

public class OutOfRangeException extends MaintenanceModelServerException{
    private final String variableName;
    private final OutOfRangeSpecifier specifier;

    public OutOfRangeException(String variableName, OutOfRangeSpecifier specifier){

        this.variableName = variableName;
        this.specifier = specifier;
    }

    public String getErrorDescription(){
        return variableName + " is " + specifier;
    }
}
