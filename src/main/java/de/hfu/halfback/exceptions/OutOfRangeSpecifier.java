package de.hfu.halfback.exceptions;

public enum OutOfRangeSpecifier {
    TooHigh,
    TooLow
}
