/**
 * Prosys OPC UA Java SDK
 *
 * Copyright (c) Prosys PMS Ltd., http://www.prosysopc.com.
 * All rights reserved.
 */

/**
 * DO NOT EDIT THIS FILE DIRECTLY! It is generated and will be overwritten on regeneration.
*/

package de.hfu.halfback.informationModel;

import com.prosysopc.ua.TypeDefinitionId;
import com.prosysopc.ua.nodes.Mandatory;
import com.prosysopc.ua.nodes.Optional;
  
/**
 * Wrapper for components that consist of several other components.
 */
@TypeDefinitionId("nsu=http://hs-furtwangen.de/MaintenanceModel/;i=1003")
public interface MultiComponentType extends ComponentType {
  
	public static final String COMPONENTS = "&lt;Components&gt;";

	
}
