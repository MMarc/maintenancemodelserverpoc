/**
 * Prosys OPC UA Java SDK
 *
 * Copyright (c) Prosys PMS Ltd., http://www.prosysopc.com.
 * All rights reserved.
 */

package de.hfu.halfback.informationModel.client;

import com.prosysopc.ua.TypeDefinitionId;
import com.prosysopc.ua.client.AddressSpace;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.QualifiedName;
  
/**
 * Represents a component that may generate or consume electrical power.
 */
@TypeDefinitionId("nsu=http://hs-furtwangen.de/MaintenanceModel/;i=1007")
public class PowerComponentTypeImpl extends PowerComponentTypeImplBase {
  protected PowerComponentTypeImpl(AddressSpace addressSpace, NodeId nodeId, QualifiedName browseName, LocalizedText displayName) {
    super(addressSpace, nodeId, browseName, displayName);
  }
}
