package de.hfu.halfback;

import org.opcfoundation.ua.builtintypes.NodeId;

public class MaintenanceModelNodeIds {
    public static final String MaintenanceInterface = "MaintenanceInterface";
    public static final String ComponentsList = "Components";
    public static final String SensorList = "Sensors";
    public static final String EngineComponent = "Engine";
    public static final String WorkpieceComponent = "Workpiece";
    public static final String CabinComponent = "Cabin";
    public static final String NoiseCabineSubcomponent = "CabineNoise";
    public static final String VibrationCabineSubcomponent = "CabineVibration";
    public static final String PowerSensor = "PowerSensor";
    public static final String TemperatureSensor = "TemperatureSensor";
    public static final String NoiseSensor = "NoiseSensor";
    public static final String VibrationSensor = "VibrationSensor";

    public static NodeId HasMetering(int namespaceId){
        return new NodeId(namespaceId, 4002);
    }

    public static NodeId HasImpactOn(int namespaceId){
        return new NodeId(namespaceId, 4003);
    }

    public static NodeId HasQualityIndicator(int namespaceId){
        return new NodeId(namespaceId, 4012);
    }
}
