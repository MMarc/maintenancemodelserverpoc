package de.hfu.halfback;

import com.pi4j.io.i2c.I2CFactory;
import de.hfu.halfback.collector.GPIOBusDataProvider;
import de.hfu.halfback.exceptions.ServerInitializationFailed;
import de.hfu.halfback.exceptions.ModelInitializationFailed;
import de.hfu.halfback.generation.InformationModelBuilder;
import de.hfu.halfback.generation.InformationModelNodeProvider;
import de.hfu.halfback.helper.EventGeneratorInitializer;
import de.hfu.halfback.helper.StressLevelCalculationInitializer;
import de.hfu.halfback.server.OPCUAServer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) throws ServerInitializationFailed, IOException, ModelInitializationFailed {
        org.apache.log4j.BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        OPCUAServer opcuaServer = new OPCUAServer();

        opcuaServer.initialize();

        InformationModelBuilder informationModelBuilder = new InformationModelBuilder();

        informationModelBuilder.build(opcuaServer);

        InformationModelNodeProvider nodeProvider = informationModelBuilder.getNodeProvider();

        new StressLevelCalculationInitializer(nodeProvider).initialize();

        new EventGeneratorInitializer(nodeProvider, informationModelBuilder.getNodeManagerUaNode()).initialize();

        opcuaServer.start();


        GPIOBusDataProvider gpioBusDataProvider = new GPIOBusDataProvider(nodeProvider);

        try{
            gpioBusDataProvider.initialize();
            gpioBusDataProvider.run();
        } catch (Exception ex) {
            logger.error("Failed to run GPIO data provider", ex);
        }

        logger.info("Press any key to exit");
        int s = System.in.read();
        opcuaServer.stop();
    }
}