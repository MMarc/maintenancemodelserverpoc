# Maintenance Information Model Server PoC
This repository contains the proof of concept implementation of the maintenance information model.

The structure is as followed: <br />
resources/ - XML files of the model <br />
src/ - Source code of the server <br />
lib/ - Copy Prosys Java SDK jars to this folder - these are required to build the project <br />
pom.xml - Contains the build configuration <br />
run.sh - A helper script to build and run the project

## Getting Started
This section provides a short description of how to build and run this project.

### Prerequisites
The following is required to build and run this project:
* [Maven](https://maven.apache.org/) - Build tool
* [Prosys Java OPC UA SDK](https://www.prosysopc.com/products/opc-ua-java-sdk/) - OPC UA SDK
* Copy the Prosys Java SDK jars into the **lib/** folder of the project
* [Java](https://www.java.com/) - Java installation

### Built
Simple builds as well as executions of this program can be performed with the **run.sh** script. Following commands are available:
```
$ ./run.sh build
$ ./run.sh run
$ ./run.sh buildRun
```
The output of a build is copied to the **target/** folder.
