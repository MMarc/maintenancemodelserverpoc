#!/usr/bin/env bash

ARG="${1:-build}"

BUILDCMD="mvn clean package"
EXECUTECMD="java -cp lib/*:target/monitoringServer-1.0-SNAPSHOT.jar de.hfu.halfback.App"

case "$ARG" in
"build")
    eval "$BUILDCMD"
    ;;
"run")
    eval "$EXECUTECMD"
    ;;
"buildRun" | *)
     eval "$BUILDCMD"
     eval "$EXECUTECMD"
    ;;
esac;
